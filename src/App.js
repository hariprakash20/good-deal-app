import React, { Component } from "react";
import ProductsList from "./components/ProductsList";
import getProductFromAPI from "./services/getProductFromAPI";
import "./App.css";
import Navbar from "./components/Navbar";

export class App extends Component {
  constructor(props) {
    super(props);

    this.API_STATES = {
      LOADING: "loading",
      LOADED: "loaded",
      ERROR: "error",
  }

    this.state = {
      products: [],
      status: this.API_STATES.LOADING,
    };
  }

  componentDidMount() {
    getProductFromAPI()
    .then((response) =>{ 
      this.setState({ ...this.state, products: response, status: this.API_STATES.LOADED })
    }).catch(err => {
        this.setState({ ...this.state,isLoading:false, status: this.API_STATES.ERROR });
      });
  }

  render() {
    const { products ,status} = this.state;
    return (
      <>
        <Navbar />
        {status === this.API_STATES.LOADING ? (
          <div className="loader"></div>
        ) : status === this.API_STATES.ERROR ? (
          <h1>Error occured in Fetching the data</h1>
        ) : products ?(
          <ProductsList products={products} />
        ) : (
          <h1>No products to display</h1>
        )}
      </>
    );
  }
}

export default App;
