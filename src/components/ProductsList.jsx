import React, { Component } from "react";
import ProductTile from "./ProductTile";
import './style.css'

export class ProductsList extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { products } = this.props;
    return (
      <>
        <div className="products-list">
          {products.map((product) => (
            <ProductTile product={product} key={product.id} />
          ))}
        </div>
      </>
    );
  }
}

export default ProductsList;
