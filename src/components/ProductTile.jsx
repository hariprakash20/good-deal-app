import React, { Component } from "react";
import "./style.css";

export class ProductTile extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { product } = this.props;
    return (
      <>
        <div className="card">
          <img src={product.image} alt="img" />
          <div className="product-title"><b>{product.title}</b></div>
          <div className="price">${product.price}</div>
          <div className="rating">
          <i className="fa-solid fa-star star"></i><span className="rate">{product.rating.rate}</span>
            <span className="count">({product.rating.count})</span>
          </div>
          <div className="buttons">
          <button className="add-to-cart">Add to cart</button>
          <button className="buy-now">Buy Now</button>
          </div>
        </div>
      </>
    );
  }
}

export default ProductTile;
