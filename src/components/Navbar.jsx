import React, { Component } from "react";
import "./style.css";

export class Navbar extends Component {
  render() {
    return (
      <div className="navbar">
        <div className="logo"><i className="fa-regular fa-handshake"></i>Good deal</div>
        <div className="search">
          <input type="text" />
          <button><i className="fa-solid fa-magnifying-glass"></i></button>
        </div>
        <div className="categories">
          <select name="categories" id="">
            <option value="">Categories</option>
          </select>
        </div>
        <div className="cart">
          <i className="fa-solid fa-cart-shopping"></i>
        </div>
      </div>
    );
  }
}

export default Navbar;
