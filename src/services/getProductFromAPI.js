export default function getProductFromAPI(){
    return fetch('https://fakestoreapi.com/products')
    .then(response => response.json())
    .then(response => response)
    .catch(err => {throw err});
}